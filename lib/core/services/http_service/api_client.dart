import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:lucky/core/services/http_service/exceptions/server_exception.dart';

import 'configs.dart';

@injectable
class ApiClient {
  ApiClient() {
    dio = Dio(baseOptions)
      ..interceptors.add(
        InterceptorsWrapper(
          onRequest: (
            RequestOptions options,
            RequestInterceptorHandler handler,
          ) async {
            return handler.next(options);
          },
          onError: (DioError err, ErrorInterceptorHandler handler) async {
            return handler.next(err);
          },
          onResponse: (
            Response<dynamic> response,
            ResponseInterceptorHandler handler,
          ) {
            handler.next(response);
          },
        ),
      );
  }

  late final Dio dio;

  BaseOptions get baseOptions => BaseOptions(
        baseUrl: Configs.apiBaseUrl,
      );

  Future<dynamic> get(
    String endpoint, {
    Map<String, dynamic>? queryParameters,
    bool forceRefresh = true,
    Options? options,
    CancelToken? cancelToken,
  }) async {
    try {
      final response = await dio.get<dynamic>(
        endpoint,
        cancelToken: cancelToken,
        queryParameters: queryParameters,
        options: options,
      );

      _validateResponse(response);

      return response.data;
    } on DioError catch (e) {
      _handleError(e);
    }
  }

  Future<dynamic> post(
    String endpoint, {
    Map<String, dynamic>? queryParameters,
    Map<String, dynamic>? body,
    Options? options,
  }) async {
    try {
      final response = await dio.post<dynamic>(
        endpoint,
        queryParameters: queryParameters,
        data: body,
        options: options,
      );

      _validateResponse(response);

      return Map<String, dynamic>.from(response.data as Map);
    } on DioError catch (e) {
      _handleError(e);
    }
  }

  Future<dynamic> patch(
    String endpoint, {
    Map<String, dynamic>? queryParameters,
    Map<String, dynamic>? body,
  }) async {
    try {
      final response = await dio.patch<dynamic>(
        endpoint,
        queryParameters: queryParameters,
        data: json.encode(body),
      );

      _validateResponse(response);

      return response.data;
    } on DioError catch (e) {
      _handleError(e);
    }
  }

  Future<void> delete(String endpoint) async {
    try {
      final response = await dio.delete<dynamic>(
        endpoint,
      );

      _validateResponse(response);

      return response.data;
    } on DioError catch (e) {
      _handleError(e);
    }
  }

  Future<dynamic> put(
    String endpoint, {
    Map<String, dynamic>? queryParameters,
    Map<String, dynamic>? body,
  }) async {
    try {
      final response = await dio.put<dynamic>(
        endpoint,
        queryParameters: queryParameters,
        data: json.encode(body),
      );

      _validateResponse(response);

      return response.data;
    } on DioError catch (e) {
      _handleError(e);
    }
  }

  void _validateResponse(Response<dynamic> response) {
    final statusCode = response.statusCode ?? 0;

    if (response.data == null || statusCode < 200 || statusCode > 304) {
      throw ServerException(response.statusMessage, response.statusCode ?? 0);
    }
  }

  void _handleError(DioError e) {
    if (e.type == DioErrorType.cancel) {
      return;
    }

    log('exception: ${e.response?.statusCode}');
    log('exception: ${e.response?.data}');
    final exception = ServerException.fromDio(e);

    throw exception;
  }

  Future<Response<dynamic>> _retry(RequestOptions requestOptions) async {
    final options = Options(
      method: requestOptions.method,
      headers: requestOptions.headers,
    );
    return dio.request<dynamic>(
      requestOptions.path,
      data: requestOptions.data,
      queryParameters: requestOptions.queryParameters,
      options: options,
    );
  }
}
