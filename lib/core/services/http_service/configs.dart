// To use different flavor apis
import 'package:lucky/flavors.dart';

class Configs {
  const Configs._();

  static String apiBaseUrl = currentFlavor.baseUrl;
}
