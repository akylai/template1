import 'package:flutter/material.dart';

abstract class LuckyElevation {
  static const smallDark = BoxShadow(
    color: Color.fromRGBO(0, 0, 0, 0.25),
    blurRadius: 15,
    offset: Offset(
      0, // x
      4, // y
    ),
  );
}
