import 'package:flutter/material.dart';
import 'package:lucky/ui/colors/lucky_colors.dart';

import 'package:lucky/ui/gradients/lucky_gradients.dart';
import 'package:lucky/ui/typography/font_weights.dart';

class LuckyRoundButton extends StatelessWidget {
  const LuckyRoundButton({
    super.key,
    this.icon = const SizedBox(),
    required this.onPressed,
    required this.text,
    required this.textColor,
    this.loading = false,
    this.width,
    this.gradient = LuckyGradients.gradientBlue,
    this.backgroundColor,
    this.active = true,
  });

  final VoidCallback onPressed;
  final String text;
  final Color textColor;
  final Widget icon;
  final double? width;
  final Gradient? gradient;
  final bool loading;
  final bool active;
  final Color? backgroundColor;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.circular(4),
      onTap: active ? onPressed : null,
      child: Container(
        margin: const EdgeInsets.all(3),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: backgroundColor,
          gradient: gradient,
          borderRadius: BorderRadius.circular(4),
        ),
        height: 32,
        width: width,
        child: loading
            ? const SizedBox(
                height: 15,
                width: 15,
                child: CircularProgressIndicator(
                  color: LuckyColors.positiveSuccess,
                ),
              )
            : Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  icon,
                  if (icon is SizedBox)
                    const SizedBox()
                  else
                    const SizedBox(
                      width: 5,
                    ),
                  Text(
                    text,
                    style: Theme.of(context).textTheme.labelSmall?.copyWith(
                          color: textColor,
                          fontSize: 10,
                          fontWeight: LuckyFontWeights.semiBold,
                        ),
                  ),
                ],
              ),
      ),
    );
  }
}
