import 'package:flutter/material.dart';

abstract class LuckyGradients {
  static const gradientBlue = LinearGradient(
    colors: [
      Color(0XFF006DEB),
      Color(0XFF338FEF),
    ],
  );
}
