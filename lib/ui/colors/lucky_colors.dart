import 'package:flutter/material.dart';

abstract class LuckyColors {
  static const positiveSuccess = Color(0XFF00B48E);
  static const info = Color(0xFF1081E8);
  static const warningCaution = Color(0xFFFF7B24);
  static const errorCritical = Color(0xFFFF4141);
  static const mute = Color(0xFFE6E8E9);
  static const placeHolder = Color(0xFF879AC1);
  static const successLight = Color(0xFFA2CFBD);
  static const successLightAlert = Color(0xFFE4FBF6);
  static const greyScale = Color(0xFF011727);
  static const infoLight = Color(0xFFE1F2FF);
  static const warningCautionLight = Color(0xFFFFE3D1);
  static const errorCriticalLight = Color(0xFFFFE3E3);
}
