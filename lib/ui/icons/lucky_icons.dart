abstract class LuckyIcons {
  static String path(String name) {
    return 'assets/icons/$name.svg';
  }
}
