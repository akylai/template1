import 'package:flutter/material.dart';
import 'package:lucky/ui/colors/lucky_colors.dart';
import 'package:lucky/ui/typography/lucky_text_style.dart';

abstract class LuckyLightTheme {
  static ThemeData get theme => ThemeData(
        colorScheme:
            const ColorScheme.light(primary: LuckyColors.positiveSuccess),

        primaryColor: LuckyColors.positiveSuccess,

        scaffoldBackgroundColor: LuckyColors.positiveSuccess,

        textSelectionTheme: const TextSelectionThemeData(
            // selectionHandleColor: LuckyColors.placeHolder,
            // selectionColor: LuckyColors.placeHolder,
            // cursorColor: LuckyColors.placeHolder,
            ),
        // inputDecorationTheme: _inputDecorationTheme,
        outlinedButtonTheme: const OutlinedButtonThemeData(),
        textButtonTheme: const TextButtonThemeData(),
        elevatedButtonTheme: const ElevatedButtonThemeData(
          style: ButtonStyle(),
        ),
        textTheme: TextTheme(
          displayLarge: LuckyTextStyle.head1,
          displayMedium: LuckyTextStyle.head2,
          displaySmall: LuckyTextStyle.head3,
          titleMedium: LuckyTextStyle.bodyXL,
          bodyLarge: LuckyTextStyle.bodyL,
          bodyMedium: LuckyTextStyle.bodyM,
          bodySmall: LuckyTextStyle.bodyS,
          labelSmall: LuckyTextStyle.bodyXS,
        ),
      );
}
