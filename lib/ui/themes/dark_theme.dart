import 'package:flutter/material.dart';
import 'package:lucky/ui/typography/lucky_text_style.dart';

abstract class LuckyDarkTheme {
  static ThemeData get theme => ThemeData(
        primaryColor: Colors.white,
        colorScheme: const ColorScheme.dark(),
        textTheme: TextTheme(
          displayLarge: LuckyTextStyle.head1,
          displayMedium: LuckyTextStyle.head2,
          displaySmall: LuckyTextStyle.head3,
          titleMedium: LuckyTextStyle.bodyXL,
          bodyLarge: LuckyTextStyle.bodyL,
          bodyMedium: LuckyTextStyle.bodyM,
          bodySmall: LuckyTextStyle.bodyS,
          labelSmall: LuckyTextStyle.bodyXS,
        ),
      );
}
